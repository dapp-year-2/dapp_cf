const User = require('./../model/userModel')
const jwt = require("jsonwebtoken")

const signToken = (id) => {
    return jwt.sign({ id }, process.env.JWT_SECRET, {
        expiresIn: process.env.JWT_EXPIRES_IN,
    })
}

const createSendToken = (user, statusCode, res) => {
    const token = signToken(user._id)
    const cookieOptions = {
        expires: new Date(
            Date.now() + process.env.JWT_COOKIE_EXPIRES_IN * 24 * 60 * 60 * 1000
        ),
        httpOnly: true
    }
    res.cookie("jwt", token, cookieOptions)
    res.status(statusCode).json({
        status: "success",
        token,
        data: {
            user
        }
    })
}

exports.navigate = async (req, res, next) => {
    try {
        createSendToken(req.body, 200, res)
        next()
    } catch (err) {
        res.status(500).json({ error: err.message })
    }
}

exports.getAllUser = async (req, res, next) => {
    try {
        const users = await User.find()
        res.status(200).json({ data: users, status: 'success' })
    } catch (err) {
        res.status(500).json({ error: err.message });
    }
}

exports.createUser = async (req, res, next) => {
    try {

        const user = new User({
            name: req.body.name,
            email: req.body.email,
            walletAddress: req.body.walletAddress,
            username: req.body.username,
            password: req.body.password,
            passwordConfirm: req.body.passwordConfirm,

        });

        await user.save();

        res.json({ data: user, status: 'success' })
    } catch (err) {
        res.status(500).json({ error: err.message });
    }
}

exports.getUser = async (req, res,) => {
    try {
        const user = await User.findById(req.params.id);

        res.json({ data: user, status: 'success' })
    } catch (err) {
        res.status(500).json({ error: err.message });
    }
}

exports.updateUser = async (req, res,) => {
    try {
        const user = await User.findByIdAndUpdate(req.params.id, req.body);

        res.json({ data: user, status: 'success' })
    } catch (err) {
        res.status(500).json({ error: err.message });
    }
}

exports.deleteUser = async (req, res,) => {
    try {
        const user = await User.findByIdAndDelete(req.params.id);

        res.json({ data: user, status: 'success' })
    } catch (err) {
        res.status(500).json({ error: err.message });
    }
}

