const mongoose = require('mongoose')
const dotenv = require("dotenv")
const app = require("./app")

dotenv.config({ path: "./config.env" })
const DB = process.env.DATABASE.replace(
    "PASSWORD",
    process.env.DATABASE_PASSWORD,
)

mongoose.connect(DB).then((con) => {
    console.log(con.connections)
    console.log("DB connect successful")
}).catch(error => console.log(error));

const port = 4002
app.listen(port, () => {
    console.log(`App Running on port ${port}..`)
})