

const express = require("express");
const cors = require("cors");
const path = require('path');

const app = express();
const userRouter = require('./routes/userRoutes');



// Enable CORS
app.use(cors());

app.use(express.json());

app.use('/api/v1/users', userRouter);


module.exports = app;
