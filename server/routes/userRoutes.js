const express = require('express')
const userControllers = require('./../controller/userController')
const authcontrollers = require('./../controller/authController')
// const jwt= require('jsonwebtoken')
// const User = require('./../model/userModel')


const router = express.Router()

router.post('/Signup', authcontrollers.signup)
router.post('/login', authcontrollers.login)
router.post('/logout', authcontrollers.logout)



router.post("/cookie", userControllers.navigate)

router
    .route('/')
    .get(userControllers.getAllUser)
    .post(userControllers.createUser)


router
    .route('/:id')
    .get(userControllers.getUser)
    .patch(userControllers.updateUser)
    .delete(userControllers.deleteUser)

module.exports = router
