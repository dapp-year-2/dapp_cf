import "./App.css";
import React, { useState, useEffect } from "react";
import { BrowserRouter as Router, Routes, Route } from "react-router-dom";
import "@fortawesome/fontawesome-free/css/all.min.css";
import "bootstrap/dist/css/bootstrap.min.css";

import CampaignDash from "./components/dashboard/campaign";
import Dashboard from "./components/dashboard/dashboard";
import NavbarD from "./components/dashboard/navbarD";
import UserDash from "./components/dashboard/user";
import SignupForm from "./components/signup";
import LoginForm from "./components/login";
// import NavBar from './components/user/NavBar';
import InvestorPage from "./components/user/investorhomepage";
import About from "./components/user/about";
import Compaigns from "./components/user/compaign";
import Profile from "./components/user/profile";
import InvestForm from "./components/user/investform";
import LandingPage from "./components/user/landingpage";

// import axios from 'axios';
// import { useNavigate } from 'react-router-dom';
import SettingDash from "./components/dashboard/settingDash";
import Web3 from "web3";
import { FUNDCHAIN_ADDRESS, FUNDCHAIN_ABI } from "./abi/config_fundchain.js";



function App() {
  let obj = null;
  if (document.cookie.indexOf("token=") !== -1) {
    obj = JSON.parse(document.cookie.substring(6));
    console.log("Cookie exists");
  } else {
    // Cookie not found
    console.log("Cookie does not exist");
  }
  const [campaigns, setCampaigns] = useState([]);
  const [connected, setConnected] = useState(null);
  const [contract, setContract] = useState(null);
  const connectAccount = async () => {
    try {
      if (!window.ethereum)
        throw new Error("No crypto wallet found. Please install it.");
      window.ethereum.send("eth_requestAccounts");
      const accounts = await window.ethereum.request({
        method: "eth_requestAccounts",
      });
      const selectedAccount = accounts[0];
      var obj = JSON.parse(document.cookie.substring(6));
      setConnected(
        selectedAccount.toLowerCase() === obj.walletAddress.toLowerCase()
      );
    } catch (error) {
      console.log(error);
    }
  };
  const loadBlockchainData = async () => {
    try {
      const web3 = new Web3(Web3.givenProvider || "HTTP://127.0.0.1:7545");
      const fundchainContract = new web3.eth.Contract(
        FUNDCHAIN_ABI,
        FUNDCHAIN_ADDRESS
      );
      setContract(fundchainContract);
      const campaignCount = await fundchainContract.methods
        .campaignCount()
        .call();

      const campaigns = [];

      for (let i = 1; i <= campaignCount; i++) {
        const campaign = await fundchainContract.methods.campaigns(i).call();
        campaigns.push(campaign);
      }
      setCampaigns(campaigns);
    } catch (error) {
      console.log(error);
    }
  };

  useEffect(() => {
    console.log("Loading blockchain data...");
    loadBlockchainData();
    connectAccount();
  }, []);

  const createCampaign = async (
    title,
    description,
    amount,
    deadline,
    image
  ) => {
    if (connected) {
      try {
        var obj = JSON.parse(document.cookie.substring(6));
        const receipt = await contract.methods
          .createCampaign(
            title,
            description,
            Web3.utils.toWei(amount, "Ether"),
            deadline,
            image
          )
          .send({ from: obj.walletAddress });
        window.alert("Project Created Successfully!");
        console.log(receipt);
        loadBlockchainData();
        return true;
      } catch (error) {
        console.error(error);
        window.alert("Project Creation Failed!");
        return false;
      }
    } else {
      window.alert("Please Connect to your wallet provided during signup");
    }
  };

  const makeInvestment = async (_campaignID, _amount) => {
    if (connected) {
      const amount = Web3.utils.toWei(_amount, "Ether");
      try {
        var obj = JSON.parse(document.cookie.substring(6));
        const receipt = await contract.methods
          .donateToCampaign(_campaignID)
          .send({ from: obj.walletAddress, value: amount });
        console.log(receipt);
        window.alert(
          "Investment Successful " + _amount + " ETH deducted from account"
        );
        loadBlockchainData();
        return true;
      } catch (error) {
        console.error(error);
        window.alert("Transaction failed: " + error.message);
        return false;
      }
    } else {
      window.alert("Please Connect to your wallet provided during signup");
    }
  };

  return (
   

    
    <div className="App">
      <Router>
        {!obj ? (
          <Routes>
            <Route path="/" element={<LandingPage />} />
            <Route path="/signup" element={<SignupForm />} />
            <Route path="/login" element={<LoginForm />} />
          </Routes>
        ) : obj.role === "admin" ? (
          <Routes>
            <Route
              path="/dashboard"
              element={<Dashboard allCampaigns={campaigns} />}
            />
            <Route path="/user" element={<UserDash />} />
            <Route
              path="/campaign"
              element={<CampaignDash allCampaigns={campaigns} />}
            />
            <Route path="/settings" element={<SettingDash />} />
            <Route path="/nav" element={<NavbarD />} />
          </Routes>
        ) : obj.role === "user" ? (
          
          <Routes>
            
            <Route
              path="/home"
              element={<InvestorPage allCampaigns={campaigns} />}
            />

            <Route
              path="/compaign"
              element={
                <Compaigns
                  createCampaign={createCampaign}
                  allCampaigns={campaigns}
                />
              }
            />
            <Route path="/about" element={<About />} />
            <Route path="/myprofile" element={<Profile />} />
            <Route
              path="/investform"
              element={
                <InvestForm contract={contract} invest={makeInvestment} />
              }
            />
          </Routes>
        ) : null}
      </Router>
    </div>
  );
}

export default App;
