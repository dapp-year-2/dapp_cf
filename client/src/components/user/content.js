import React from "react";
import './../assets/css/content.css';
import joinus from './../assets/img/joinus.png'

function Content() {
  return (
    <div>
      <div className='fund_raise'>
        <div><h4><b>Fundraising on ChainFund takes <br></br> just a few minutes</b></h4></div>
          < div className='fundraise'>  
            <div className='start_fund'>
              <p>1</p>
              <h5><b>Start with the basics</b></h5>
              <h6>Kick things off with your name and location.</h6>
            </div>
            <div className='start_fund'>
              <p>2</p>
              <h5><b>Tell Your story</b></h5>
              <h6>Tell people about the cause you want to fundraise for.</h6>
            </div>
            <div className='start_fund'>
              <p>3</p>
              <h5><b>Share with the rest of the world</b></h5>
              <h6>People out there want to help you.</h6>
            </div>
          </div>
      </div>
          <div className="invest">
            <div><h4><b>How to Invest</b></h4></div>
            <div className="invests">
              <div>
                <p>1</p>
                <h3><b>Sign Up</b></h3>
                <h6>Securely create an account on FundChain.</h6>
              </div>
              <div>
                <p>2</p>
                <h3><b>Browse Projects</b></h3>
                <h6>Review hundreds of investment opportunities  <br></br>from startups to collectibles.</h6>
              </div>
              <div>
                <p>3</p>
                <h3><b>Make an Investment</b></h3>
                <h6>Submit your payment and own a financial stake <br></br> in a startup or collectible.</h6>
              </div>
            </div>
          </div>  
          <div className='project'>
            <div className='find_project'>
              <h5>Ready to get started? Join thousands of others today</h5>
              <button className="prjbutton">Start a Project</button>
              <button className="fundbutton">Fund a Project</button>
            </div>
            <div className='find_project'>
            <img src={joinus} alt=""></img>
            </div>
          </div>
    </div>
  );
}

export default Content;