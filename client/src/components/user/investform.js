import React, { useState, useEffect } from "react";
import { Container } from "react-bootstrap";
import './../assets/css/invest.css'
import NavBar from "./NavBar";

function InvestForm(props) {
    const [campaign, setCampaign] = useState(null);
    const [amount, setAmount] = useState("")
    const [amountErr, setAmountErr] = useState("");
    const [formCheck, setFormCheck] = useState(false)
    var obj = JSON.parse(document.cookie.substring(6))
    useEffect(() => {
        if (!props.contract) {
            console.error("Contract object is null");
            return;
        }
        const getCampaignData = async () => {
            try {
                const result = await props.contract.methods.getCampaign(obj.campaign_id).call();
                // console.log(result)
                setCampaign(result);
            } catch (error) {
                console.error(error);
            }
        };
        getCampaignData();
    }, [props.contract, obj.campaign_id]);
    useEffect(() => {
        var c = 0;
        if (!amount) {
            setAmountErr("Provide an amount")
            c += 1;
        } else if (amount <= 0) {
            setAmountErr("Fundraiser must be greater than 0 ETH");
            c += 1;
        } else {
            setAmountErr("")
        }
        c === 0 ? setFormCheck(true) : setFormCheck(false)
    })
    const handleAmount = (e) => {
        const regex = /^[0-9]*\.?[0-9]*$/;
        const newValue = e.target.value;
        if (regex.test(newValue)) {
            setAmount(newValue);
        }
    }

    const handleClick = async () => {
        if (!formCheck) {
            window.alert("Please provide a valid amount")
            return;
        }
        if (campaign[0].toLowerCase() === obj.walletAddress.toLowerCase()) {
            window.alert("You cannot donate to your own campaign!")
            return;
        }
        try {
            const res = await props.invest(obj.campaign_id, amount)
            res === true ? console.log("success") : console.log("fail")
        } catch (err) {
            window.alert("Error, please check your console")
            console.log(err)
        }
    };

    if (!campaign) {
        return <div>Loading campaign details...</div>;
    }

    return (
        <>
            <NavBar />
            <section className="investpicture" id="home">
                <Container className="items">
                    <h2>Help us bring our dream to life!</h2>
                    <h6>Let's build something amazing together!</h6>
                </Container>
            </section>
            <section style={{ display: "flex", justifyContent: "center" }}>
                <div className="investMine">
                    <h5>Invest</h5>
                    {campaign[6] === true ? <h3 style={{ textAlign: "center", fontSize: "1.15rem", color: "#8c8c8c" }}>This campaign is complete, and no further investments are accepted</h3> : null}
                    <div className="invest-items border border-dark">
                        <h4><strong>{campaign[1]}</strong></h4>
                        <img src={campaign[7]} alt="Loading ..." ></img>
                        <div>
                            {campaign[2].split(/\n|\n\n/).map((p, i) => (
                                <p key={i}>{p}</p>
                            ))}
                            <p><strong>Amount Raised:</strong> {campaign[4] / 1e18} ETH</p>
                            <p><strong>Target Amount:</strong> {campaign[3] / 1e18} ETH</p>

                            <div style={{ marginBlock: "1rem", display: "flex", flexDirection: "column", gap: "0.6rem" }}>
                                <input onChange={(e) => { handleAmount(e) }} type="text" placeholder="0.0 ETH" value={amount} style={{ display: "block" }} />
                                {amountErr && <span style={{ color: "red", fontSize: "0.7rem", fontWeight: 600, opacity: 0.7 }}>{amountErr}</span>}
                                <button className="btn-invest" onClick={handleClick}>Invest</button>
                            </div>
                        </div>
                    </div>
                </div>
            </section>
        </>
    )
} export default InvestForm;