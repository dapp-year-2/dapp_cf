import React from 'react';
import { Navbar, Container, Nav } from 'react-bootstrap';
import whiteLogo from './../assets/img/logo.png';
import { Link } from 'react-router-dom';
import Profile from '../assets/img/Screenshot 2023-06-11 233130.png';
import './../assets/css/nav.css';

function NavBar() {
  return (
    <Navbar bg="white" expand="lg">
      <Container>
        <Navbar.Brand as={Link} to="/">
          <img
            src={whiteLogo}
            height="40"
            className="d-inline-block align-top logo"
            alt="React Bootstrap logo"
          />
        </Navbar.Brand>
        <Navbar.Toggle aria-controls="basic-navbar-nav" />
        <Navbar.Collapse id="basic-navbar-nav" className="navi">
          <Nav className="mr-auto">
            <Nav.Link as={Link} to="/home" className="mx-3 navi-link">
              Home
            </Nav.Link>
            <Nav.Link as={Link} to="/compaign" className="mx-3 navi-link">
              Compaign
            </Nav.Link>
            <Nav.Link as={Link} to="/about" className="mx-3 navi-link">
              About
            </Nav.Link>
          </Nav>
        </Navbar.Collapse>
        <Link to="/myprofile">
          <img
            height="40px"
            width="40px"
            src={Profile}
            alt="profilePic"
            className="profilePic"
          />
        </Link>
      </Container>
    </Navbar>
  );
}

export default NavBar;
