import React, { useState, useEffect } from "react";
import { Container, Form, Button, Col, Row } from "react-bootstrap";
import './../assets/css/compaign.css'
import NavBar from "./NavBar";
import Axios from "axios";
import { useNavigate } from "react-router-dom";
// import NavBar from './NavBar';


function Compaigns(props) {
    const [formCheck, setFormCheck] = useState(false)
    const [title, setTitle] = useState("")
    const [description, setDescription] = useState("")
    const [amount, setAmount] = useState("")
    const [deadline, setDeadline] = useState("")
    const [image, setImage] = useState("")
    const [amountErr, setAmountErr] = useState("")
    const [descriptionErr, setDescriptionErr] = useState("")
    const navigate = useNavigate()
    var obj = JSON.parse(document.cookie.substring(6))
    let myCampaigns = [];
    for (var i = 0; i < props.allCampaigns.length; i++) {
        if (props.allCampaigns[i].owner.toLowerCase() === obj.walletAddress.toLowerCase()) {
            myCampaigns.push([props.allCampaigns[i], i + 1])
        }
    }


    useEffect(() => {
        var c = 0;
        if (!description) {
            setDescriptionErr("")
        } else if (description.length <= 50) {
            setDescriptionErr("To help donors comprehend the cause you are fundraising for, kindly submit descriptions longer than 50 characters.")
            c += 1;
        } else {
            setDescriptionErr("")
        }

        if (!amount) {
            setAmountErr("")
        } else if (amount <= 0) {
            setAmountErr("Fundraiser must be greater than 0 ETH");
            c += 1;
        } else {
            setAmountErr("")
        }

        c === 0 ? setFormCheck(true) : setFormCheck(false)
    })

    function handleAmount(e) {
        const regex = /^[0-9]*\.?[0-9]*$/;
        const newValue = e.target.value;
        if (regex.test(newValue)) {
            setAmount(newValue);
        }
    }

    const handleSubmit = async () => {
        var today = new Date().toISOString().split('T')[0];
        if (deadline < today) {
            window.alert("Please select a date after today.");
            return;
        }
        try {
            const res = await props.createCampaign(title, description, amount, Date.parse(deadline) / 1000, image)
            res === true ? console.log("success") : console.log("fail")
        } catch (err) {
            window.alert("Error, please check your console");
            console.log(err);
        }
    }


    function calcProgress(amountRaised, targetAmount) {
        return ((amountRaised / targetAmount) * 100).toFixed(2);
    }

    function calcProgressbar(amountRaised, targetAmount) {
        let percent = ((amountRaised / targetAmount) * 100).toFixed(2);
        if (percent >= 100) return 100;
        return percent;
    }

    const navigation = async (campaignID) => {
        var cookie = JSON.parse(document.cookie.substring(6))
        cookie["campaign_id"] = campaignID;
        console.log(cookie)
        try {
            const res = await Axios({
                method: "POST",
                url: "http://localhost:4002/api/v1/users/cookie",
                data: cookie
            })
            if (res.data.status === "success") {
                var obj = res.data.data.user
                document.cookie = "token=" + JSON.stringify(obj) + "; SameSite=None; secure=true; path=/";
                navigate('/investform');
                window.scrollTo(0, 0);
            }
        } catch (err) {
            window.alert(err)
        }
    }

    return (
        <>
            <NavBar />
            <section className="compaignpicture" id="home">
                <Container className="items">
                    <h2>About ChainFund
                    </h2>
                    <h6>A Blockchain-based CrowdFunding Platform for Everyone</h6>
                </Container>
            </section>
            <div className='bg-white '>
                <h3>Create a Compaign</h3>
                <div className='fundraise_form form_border bg-white'>
                    <Form onSubmit={(e) => {
                        e.preventDefault()
                        if (!formCheck) {
                            window.alert("Please fix all the errors before proceeding")
                            return;
                        }
                        handleSubmit()
                    }}>
                        <Row className='mb-3'>
                            <Col md={12}>
                                <Form.Group>
                                    <Form.Control required type="text" placeholder="Project Title" onChange={(e) => setTitle(e.target.value)} />
                                </Form.Group>
                            </Col>
                        </Row>
                        <Row className='mb-3'>
                            <Col md={12}>
                                <Form.Group>
                                    <Form.Control required as="textarea" rows={3} placeholder="Explain for what and why you want to fundraise" onChange={(e) => setDescription(e.target.value)} />
                                    {descriptionErr && <span style={{ color: "red", fontSize: "0.7rem", marginBlock: "1rem", fontWeight: 600, opacity: 0.7 }}>{descriptionErr}</span>}
                                </Form.Group>
                            </Col>
                        </Row>
                        <Row className='mb-3'>
                            <Col md={12}>
                                <Form.Group>
                                    <Form.Control required type="text" value={amount} placeholder="Fundraiser goal (ex. 1ETH)" onChange={(e) => handleAmount(e)} />
                                    {amountErr && <span style={{ color: "red", fontSize: "0.7rem", marginBlock: "1rem", fontWeight: 600, opacity: 0.7 }}>{amountErr}</span>}
                                </Form.Group>
                            </Col>
                        </Row >
                        <Row className='mb-3'>
                            <Col md={12}>
                                <Form.Group>
                                    <Form.Label>End Date</Form.Label>
                                    <Form.Control required type="date" placeholder="End date" onChange={(e) => setDeadline(e.target.value)} />
                                </Form.Group>
                            </Col>
                        </Row >
                        <Row className='mb-3'>
                            <Col md={12}>
                                <Form.Group>
                                    <Form.Label>Image</Form.Label>
                                    <Form.Control required type="text" placeholder='Provide image URL for your campaign' onChange={(e) => setImage(e.target.value)} />
                                </Form.Group>
                            </Col>
                        </Row>
                        <Button variant="primary" type="submit">
                            Create a campaign
                        </Button>
                    </Form>
                </div>
                <div className="mycompaigns">
                    <hr></hr>
                    <h4>My Campaigns</h4>
                    <Row>
                        {myCampaigns.map((campaign, i) => (
                            <Col key={i} md={6} lg={4}>
                                <div className="prj-item">
                                    <img src={campaign[0].image} alt="item-img" />
                                    <div className="prj-desc">
                                        <h5>{campaign[0].title}</h5>
                                        <p>{campaign[0].description.slice(0, 100)} ...</p>
                                        <div className="progres">
                                            <span>{calcProgress(campaign[0].amountRaised / 1e18, campaign[0].targetAmount / 1e18)}%</span>
                                            <div className="p-bar"><div className="progres-bar" style={{ width: `${calcProgressbar(campaign[0].amountRaised / 1e18, campaign[0].targetAmount / 1e18)}%` }}></div></div>
                                            <p><strong>{campaign[0].amountRaised / 1e18} ETH</strong> raised out of <strong>{campaign[0].targetAmount / 1e18} ETH</strong></p>
                                        </div>
                                    </div>
                                    <button className="myBTN" onClick={(e) => navigation(campaign[1])}>View</button>
                                </div>
                            </Col>
                        ))}
                    </Row>
                </div>
            </div>
        </>

    )
} export default Compaigns