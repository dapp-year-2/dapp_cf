import React from "react";
import about1 from '../assets/img/about1.png'
import about2 from '../assets/img/about2.png'
import about3 from '../assets/img/about3.png'
import about4 from '../assets/img/about4.png'
import about5 from '../assets/img/about5.png'
import './../assets/css/about.css'
import { Container } from "react-bootstrap";
import NavBar from './NavBar';


function About() {
  return (
    <><NavBar/>
      <section className="aboutpicture" id="home">
        <Container className="items">
          <h2>About ChainFund</h2>
          <h6>A Blockchain-based CrowdFunding Platform for Everyone</h6>
        </Container>
      </section>
      <div className="works">
        <h4>How it works?</h4>
        <div className="working">
          <div className="working_items">
            <img src={about1} alt=""></img>
          </div>
          <div className="working_items">
            <p>
              Blockchain-based crowdfunding works by leveraging the unique
              features of blockchain technology, such as immutability,
              transparency, and decentralization, to enable individuals and
              organizations to raise funds from a large number of investors
              without the need for intermediaries.
            </p>
            <button>Learn more</button>
          </div>
        </div>
      </div>
      <div className="benefit">
        <h4>Benefits of ChainFund</h4>
        <div className="benefits">
          <div className="benefits_items">
            <p>
              Blockchain-based crowdfunding works by leveraging the unique
              features of blockchain technology, such as immutability,
              transparency, and decentralization, to enable individuals and
              organizations to raise funds from a large number of investors
              without the need for intermediaries.
            </p>
          </div>
          <div className="benefits_items">
            <img src={about2} height={290} width={400} alt=""></img>
          </div>
        </div>
      </div>
      <div className="completed">
        <h4>Campaign completed</h4>
        <div className="completes">
          <div className="completes_items">
            <img src={about3} height={200} width={300} alt=""></img>
            <p>Campaign Red for Red has raised over $500 million dollars.</p>
          </div>
          <div className="completes_items">
            <img src={about4} height={200} width={300} alt=""></img>
            <p>Campaign inspiring Women in 2023 has raised over $1 million dollars.</p>
          </div>
          <div className="completes_items">
            <img src={about5} height={200} width={300} alt=""></img>
            <p>Campaign “The Conflict Campaign” has raised over $5 million dollars.</p>
          </div>
        </div>
        <div className="button">
          <button>Create your campaign</button>
          <button>Fund Campaign</button>
        </div>
      </div>
    </>
  );
}

export default About;
