// import "./../assets/css/footer.css"
// import { IoLogoFacebook, IoLogoInstagram, IoLogoTwitter } from 'react-icons/io5';
// export default function Footer() {
//     return (
//         <>
//             <div className="myfooter bg-dark">
//                 <div className="column about">
//                     <h2>About ChainFund</h2>
//                     <p>We leverage Ethereum and Blockchain technology to fund global projects with high environmental and public health impact using a radically transparent donation platform.</p>
//                 </div>
//                 <div className="column line"></div>
//                 <div className="column contact">
//                     <h3>Contact Us</h3>
//                     <div>
//                         <h5>General</h5>
//                         <p>xeviabcd28@gmail.com</p>
//                     </div>
//                     <div>
//                         <h5>Technical Support</h5>
//                         <p>dev.gcit@gmail.com</p>
//                     </div>
//                 </div>
//                 <div className="column line"></div>
//                 <div className="column info">
//                     <h3>Information</h3>
//                     <div>
//                         <h5>FAQs</h5>
//                         <h5>Terms of Use</h5>
//                         <h5>Privacy Policy</h5>
//                     </div>
//                 </div>
//                 <div className="column line"></div>
//                 <div className="column follow">
//                     <h3>Follow Us</h3>
//                     <div className="logos">
//                         <IoLogoFacebook size={32} color="white" />
//                         <IoLogoTwitter size={32} color="white" />
//                         <IoLogoInstagram size={32} color="white" />
//                     </div>
//                     <div className="copyright">
//                         Copyright 2023<br></br>All Rights Reserved by CryptoCares
//                     </div>
//                 </div>
//             </div>
//         </>
//     )
// }


import React from "react";
import { IoLogoFacebook, IoLogoInstagram, IoLogoTwitter } from 'react-icons/io5';
import { Container, Row, Col } from "react-bootstrap";
import "./../assets/css/footer.css";

export default function Footer() {
  return (
    <div className="myfooter bg-dark">
      <Container>
        <Row>
          <Col xs={12} md={6} lg={3} className="column about">
            <h2>About ChainFund</h2>
            <p>We leverage Ethereum and Blockchain technology to fund global projects with high environmental and public health impact using a radically transparent donation platform.</p>
          </Col>
          <Col xs={12} md={6} lg={3} className="column contact">
            <h3>Contact Us</h3>
            <div>
              <h5>General</h5>
              <p>xeviabcd28@gmail.com</p>
            </div>
            <div>
              <h5>Technical Support</h5>
              <p>dev.gcit@gmail.com</p>
            </div>
          </Col>
          <Col xs={12} md={6} lg={3} className="column info">
            <h3>Information</h3>
            <div>
              <h5>FAQs</h5>
              <h5>Terms of Use</h5>
              <h5>Privacy Policy</h5>
            </div>
          </Col>
          <Col xs={12} md={6} lg={3} className="column follow">
            <h3>Follow Us</h3>
            <div className="logos">
              <IoLogoFacebook size={32} color="white" />
              <IoLogoTwitter size={32} color="white" />
              <IoLogoInstagram size={32} color="white" />
            </div>
            <div className="copyright">
              Copyright 2023
              <br />
              All Rights Reserved by CryptoCares
            </div>
          </Col>
        </Row>
      </Container>
    </div>
  );
}
