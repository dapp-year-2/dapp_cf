import React from "react";
import { Container } from "react-bootstrap";
import './../assets/css/banner.css'
function Banner() {
  return (
    <section className="banner" id="home">
      <Container className="items">
        <h2>Join Us in Bringing Your Projects <br></br>to Life<br></br>
        </h2>
        <h6>Connect with other people and empower creatives by  supporting independent  <br></br>artists and creators</h6>
        <a className="joinnow" href="joinnow.js"><button className="joinnow">Join Now</button></a>
      </Container>
    </section>

  )
} export default Banner;