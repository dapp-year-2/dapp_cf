import React from "react";
import { Form, Button, Container, Row, Col } from "react-bootstrap";
import { FaSearch } from 'react-icons/fa';
import './../assets/css/investorpage.css';
import { useNavigate } from "react-router-dom";
import Axios from "axios";
import NavBar from "./NavBar";

function Investpage(props) {
  const navigate = useNavigate()
  let campaigns = [];
  for (var i = 0; i < props.allCampaigns.length; i++) {
    campaigns.push([props.allCampaigns[i], i + 1])
  }
  // console.log(campaigns)

  function calcProgress(amountRaised, targetAmount) {
    return ((amountRaised / targetAmount) * 100).toFixed(2);
  }

  function calcProgressbar(amountRaised, targetAmount) {
    let percent = ((amountRaised / targetAmount) * 100).toFixed(2);
    if (percent >= 100) return 100;
    return percent;
  }

  const navigation = async (campaignID) => {
    var cookie = JSON.parse(document.cookie.substring(6))
    cookie["campaign_id"] = campaignID;
    console.log(cookie)
    try {
      const res = await Axios({
        method: "POST",
        url: "http://localhost:4002/api/v1/users/cookie",
        data: cookie
      })
      if (res.data.status === "success") {
        var obj = res.data.data.user
        document.cookie = "token=" + JSON.stringify(obj) + "; SameSite=None; secure=true; path=/";
        navigate('/investform');
        window.scrollTo(0, 0);
      }
    } catch (err) {
      window.alert(err)
    }
  }

  return (
    
    <>
    <NavBar/>
      <section className="homepicture" id="home">
        <Container className="items">
          <h2>About ChainFund</h2>
          <h6>A Blockchain-based CrowdFunding Platform for Everyone</h6>
        </Container>
      </section>

      <Container className="my-4">
        <Row>
          <Col md={{ span: 6, offset: 3 }}>
            <Form className="d-flex">
              <Form.Control
                type="search"
                placeholder="Search for campaigns"
                className="py-2 px-3"
                aria-label="Search"
              />
              <Button className="search-btn" variant="outline-primary">
                <FaSearch />
              </Button>
            </Form>
          </Col>
        </Row>
      </Container>

      <section className="exploreSection">
        <Container>
          <h2>All Projects</h2>
          <Row>
            {campaigns.map((campaign, i) => (
              <Col key={i} md={6} lg={4}>
                <div className="prj-item">
                  <img src={campaign[0].image} alt="item-img" />
                  <div className="prj-desc">
                    <h5>{campaign[0].title}</h5>
                    <p>{campaign[0].description.slice(0, 100)} ...</p>
                    <div className="progres">
                      <span>{calcProgress(campaign[0].amountRaised / 1e18, campaign[0].targetAmount / 1e18)}%</span>
                      <div className="p-bar"><div className="progres-bar" style={{ width: `${calcProgressbar(campaign[0].amountRaised / 1e18, campaign[0].targetAmount / 1e18)}%` }}></div></div>
                      <p><strong>{campaign[0].amountRaised / 1e18} ETH</strong> raised out of <strong>{campaign[0].targetAmount / 1e18} ETH</strong></p>
                    </div>
                  </div>
                  <button className="myBTN" onClick={(e) => navigation(campaign[1])}>View</button>
                </div>
              </Col>
            ))}
          </Row>
        </Container>
      </section>
    </>
  )
}

export default Investpage;


