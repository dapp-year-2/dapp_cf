// export default LoginForm;
import React, { useState, useEffect } from "react";
import "./css/login.css";
import dappLogo from "./img/dapplogo.png";
import axios from "axios";
import { Link, } from "react-router-dom";

function LoginForm() {
  const [email, setEmail] = useState("");
  const [password, setPassword] = useState("");
  const [errors, setErrors] = useState({});
  const [success, setSuccess] = useState(false);
  // const navigate = useNavigate();

  const handleEmailChange = (event) => {
    setEmail(event.target.value);
  };

  const handlePasswordChange = (event) => {
    setPassword(event.target.value);
  };

  const handleSubmit = (event) => {
    event.preventDefault();

    // Form validation
    const errors = {};

    if (!email) {
      errors.email = "Email is required";
    } else if (!/\S+@\S+\.\S+/.test(email)) {
      errors.email = "Invalid email address";
    }

    if (!password) {
      errors.password = "Password is required";
    } else if (password.length < 6) {
      errors.password = "Password must be at least 6 characters long";
    }

    if (Object.keys(errors).length === 0) {
      const formData = { email, password };
      axios
        .post("http://localhost:4002/api/v1/users/login", formData)
        .then((response) => {
          console.log(response);
          if (response.data.status === "success") {
            var obj = response.data.data.user;
            console.log(obj);
            document.cookie =
              "token=" +
              JSON.stringify(obj) +
              "; SameSite=None; secure=true; path=/";
            console.log(response.data);
            console.log("role:", response.data.data.user.role);
            if (response.data.data.user.role === "admin") {
              console.log("Navigating to /dashboard");
              window.location.assign("/dashboard");
              console.log("Login successful");
              alert("success");
            } else {
              console.log("Navigating to /home");
              window.location.assign("/home");
            }
          } else {
            console.log("Login failed");
          }
        })
        .catch((error) => {
          console.log(error);
        });
    } else {
      // Form is invalid, display the errors
      setErrors(errors);
      setSuccess(false);
    }
  };

  useEffect(() => {
    // Clear messages after 3 seconds
    const timer = setTimeout(() => {
      setErrors({});
      setSuccess(false);
    }, 3000);

    // Clear the timer if the component is unmounted or email/password is changed
    return () => clearTimeout(timer);
  }, [email, password]);

  return (
    <div className="SignupAll">
      <div className="headingSign">
        <img src={dappLogo} id="LogoSign" alt="Logo" />
        <Link to="/signup" id="LoginHead1">
          Sign up
        </Link>
      </div>
      <h1 id="headL">Login</h1>
      <form id="loginForm" onSubmit={handleSubmit}>
        <label htmlFor="email">Email</label>
        <br />
        <input
          type="email"
          name="email"
          value={email}
          onChange={handleEmailChange}
        />
        <br />
        <label htmlFor="pword">Password</label>
        <br />
        <input
          type="password"
          name="pword"
          value={password}
          onChange={handlePasswordChange}
        />
        <br />

        {Object.keys(errors).length > 0 && (
          <div className="messageContainer errorContainer">
            {Object.values(errors).map((error, index) => (
              <React.Fragment key={index}>
                <span className="error">{error}</span>
                {index !== Object.keys(errors).length - 1 && <br />}
              </React.Fragment>
            ))}
          </div>
        )}

        {success && (
          <div className="messageContainer successContainer">
            <span className="success">Form submitted successfully!</span>
          </div>
        )}

        <input type="submit" id="logSubmit" value="Submit" />
        <p>
          Don't have an account? <Link to="/signup">Signup</Link>
        </p>
      </form>
    </div>
  );
}

export default LoginForm;
