

import React, { useState } from "react";
import "./css/signup.css";
import dappLogo from "./img/dapplogo.png";
import axios from 'axios';

function SignupForm() {
  const [formData, setFormData] = useState({
    name: "",
    email: "",
    walletAddress:"",
    username: "",
    password: "",
    passwordConfirm: "",
  });

  const handleChange = (e) => {
    setFormData({ ...formData, [e.target.name]: e.target.value });
  };

  const handleSubmit = (e) => {
    e.preventDefault();

    axios.post('http://localhost:4002/api/v1/users/signup', formData)
      .then(response => {
        // Handle the response
        console.log(response);
        if (response.data.status === "success") {
          console.log("Signup successful");
          alert("Success")
          window.location.href = "/login";

          // Redirect to another page, display a success message, etc.
        } else {
          console.log("Signup failed");
          // Display an error message, handle the failure scenario, etc.
        }
      })
      .catch((error) => {
        console.log(error);
        // Handle the error
      });
     
  };

  return (
    <div className="SignupAll">
      <div className="headingSign">
        <img src={dappLogo} id="LogoSign" alt="Dapp Logo" />
        <button id="LoginHead1">Login</button>
      </div>
      <h1 id="headS">Join Us</h1>
      <form id="signupForm" onSubmit={handleSubmit}>
        <label htmlFor="name">Name:</label>
        <br />
        <input
          type="text"
          name="name"
          value={formData.name}
          onChange={handleChange}
        />
        <br />
        <label htmlFor="email">Email</label>
        <br />
        <input
          type="email"
          name="email"
          value={formData.email}
          onChange={handleChange}
        />
        <br />
        <label htmlfor="walletAddress">Wallet Address</label>
        <br />
         <input 
           type="text" 
           name="walletAddress"
           value={formData.walletAddress} 
           onChange={handleChange}/><br />
        <label htmlFor="username">Username</label>
        <br />
        <input
          type="text"
          name="username"
          value={formData.username}
          onChange={handleChange}
        />
        <br />
        <label htmlFor="password">Password</label>
        <br />
        <input
          type="password"
          name="password"
          value={formData.password}
          onChange={handleChange}
        />
        <br />
        <label htmlFor="passwordConfirm">Confirm Password</label>
        <br />
        <input
          type="password"
          name="passwordConfirm"
          value={formData.passwordConfirm}
          onChange={handleChange}
        />
        <br />

        <input type="submit" id="signSubmit" value="Submit" />
        <p>
          Already have an account? <a href="/login">Login</a>
        </p>
      </form>
    </div>
  );
}

export default SignupForm;
