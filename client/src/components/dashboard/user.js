


import React, { useEffect, useState } from 'react';
import axios from 'axios';
import './css/user.css';
import Illustration from '../img/Illustration.png';
import NavbarD from './navbarD';

const UserDash = () => {
  const [users, setUsers] = useState([]);

  useEffect(() => {
    fetchUsers();
  }, []);

  const fetchUsers = async () => {
    try {
      const response = await axios.get('http://localhost:4002/api/v1/users');
      setUsers(response.data.data);
    } catch (error) {
      console.log(error);
    }
  };

  const handleDeleteUser = async (userId) => {
    const confirmed = window.confirm('Are you sure you want to delete this user?');
    if (confirmed) {
      try {
        await axios.delete(`http://localhost:4002/api/v1/users/${userId}`);
        setUsers((prevUsers) => prevUsers.filter((user) => user._id !== userId));
      } catch (error) {
        console.log(error);
      }
    }
  };

  const userCount = users.length;

  return (
    <div className="DashAll">
      <NavbarD />

      <div className="HeadingAd1">
        <h1>Hello Admin</h1>
        <p>It's good to see you again!</p>
      </div>

      <img src={Illustration} id="imgill1" alt="Illustration" />

      <div className="headingDD1" id="hd11">
        <h1>{userCount}</h1>
        <p>Total</p>
        <p>Users</p>
      </div>

      <h2 id="Ttitle21">User</h2>

      <table className="DashTable1" id="UserTable1">
        <thead>
          <tr>
            <th></th>
            <th>Name</th>
            <th></th>
          </tr>
        </thead>

        <tbody>
          {users.map((user, index) => (
            <tr key={index}>
              <td>
                {/* <img src={user.profilePic} alt="Profile" style={{ width: '70px', height: '50px', borderRadius: '10px' }} /> */}
              </td>
              <td>
                <h4>{user.name}</h4>
                <p>{user.email}</p>
              </td>
              <td> <button onClick={() => handleDeleteUser(user._id)}>Delete</button> </td>
            </tr>
          ))}
        </tbody>
      </table>
    </div>
  );
};

export default UserDash;
