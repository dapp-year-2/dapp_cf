
// import React, { useState } from 'react';
// import { useNavigate } from 'react-router-dom';
// import "./css/navbarD.css";
// import CF from "../img/CF..png";

// import { FaHome, FaUser, FaEnvelope, FaCog, FaSignOutAlt } from 'react-icons/fa';

// const NavbarD = () => {
//   const [showConfirmation, setShowConfirmation] = useState(false);
//   const navigate = useNavigate();

//   const navigateTo = (route) => {
//     navigate(route);
//   };

//   const handleLogout = () => {
//     setShowConfirmation(true);
//   };

//   const handleConfirmLogout = () => {
//     localStorage.removeItem('token');

//     navigateTo('/');
//   };

//   const handleCancelLogout = () => {
//     setShowConfirmation(false);
//   };

//   return (
//     <div className='navbarD'>
//       <nav>
//         <ul>
//           <img src={CF} id='CFlogo' alt='' />
//           <li>
//             <button onClick={() => navigateTo('/dashboard')}>
//               <FaHome className='react_icon' />
//             </button>
//           </li>
//           <li>
//             <button onClick={() => navigateTo('/user')}>
//               <FaUser className='react_icon' />
//             </button>
//           </li>
//           <li>
//             <button onClick={() => navigateTo('/campaign')}>
//               <FaEnvelope className='react_icon' />
//             </button>
//           </li>
//           <li>
//             <button onClick={() => navigateTo('/settings')}>
//               <FaCog className='react_icon' />
//             </button>
//           </li>
//           <li>
//             <button onClick={handleLogout}>
//               <FaSignOutAlt className='react_icon' />
//             </button>
//           </li>
//         </ul>
//       </nav>
//       {showConfirmation && (
//         <div className="confirmation-dialog">
//           <p>Are you sure you want to log out?</p>
//           <div className="button-container">
//             <button onClick={handleConfirmLogout}>Yes</button>
//             <button onClick={handleCancelLogout}>No</button>
//           </div>
//         </div>
//       )}
//     </div>
//   );
// };

// export default NavbarD;
import React, { useState } from 'react';
import { useNavigate } from 'react-router-dom';
import "./css/navbarD.css";
import CF from "../img/CF..png";

import { FaHome, FaUser, FaEnvelope, FaCog, FaSignOutAlt } from 'react-icons/fa';

const NavbarD = () => {
  const [showConfirmation, setShowConfirmation] = useState(false);
  const navigate = useNavigate();

  const navigateTo = (route) => {
    navigate(route);
  };

  const handleLogout = () => {
    setShowConfirmation(true);
  };

  const handleConfirmLogout = () => {
    // Clear all cookies
    const cookies = document.cookie.split(";");

    for (let i = 0; i < cookies.length; i++) {
      const cookie = cookies[i];
      const eqPos = cookie.indexOf("=");
      const name = eqPos > -1 ? cookie.substr(0, eqPos) : cookie;
      document.cookie = `${name}=;expires=Thu, 01 Jan 1970 00:00:00 GMT;path=/;`;
    }

    navigateTo('/');
  };

  const handleCancelLogout = () => {
    setShowConfirmation(false);
  };

  return (
    <div className='navbarD'>
      <nav>
        <ul>
          <img src={CF} id='CFlogo' alt='' />
          <li>
            <button onClick={() => navigateTo('/dashboard')}>
              <FaHome className='react_icon' />
            </button>
          </li>
          <li>
            <button onClick={() => navigateTo('/user')}>
              <FaUser className='react_icon' />
            </button>
          </li>
          <li>
            <button onClick={() => navigateTo('/campaign')}>
              <FaEnvelope className='react_icon' />
            </button>
          </li>
          <li>
            <button onClick={() => navigateTo('/settings')}>
              <FaCog className='react_icon' />
            </button>
          </li>
          <li>
            <button onClick={handleLogout}>
              <FaSignOutAlt className='react_icon' />
            </button>
          </li>
        </ul>
      </nav>
      {showConfirmation && (
        <div className="confirmation-dialog">
          <p>Are you sure you want to log out?</p>
          <div className="button-container">
            <button onClick={handleConfirmLogout}>Yes</button>
            <button onClick={handleCancelLogout}>No</button>
          </div>
        </div>
      )}
    </div>
  );
};

export default NavbarD;
