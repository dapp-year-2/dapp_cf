
import React from 'react';
import axios from 'axios';
import './css/setting.css';
import NavbarD from './navbarD';
import bcrypt from 'bcryptjs';


const SettingDash = () => {
  let obj = null;
  if (document.cookie.indexOf("token=") !== -1) {
    obj = JSON.parse(document.cookie.substring(6));
    console.log("Cookie exists");
  } else {
    // Cookie not found
    console.log("Cookie does not exist");
  }

  console.log(obj._id);
  const userId = obj._id;

  const handleSubmit = async (event) => {
    event.preventDefault();

    const formData = new FormData(event.target);
    const name = formData.get('name');
    const email = formData.get('email');
    const newPassword = formData.get('password');
    const confirmPassword = formData.get('Cpassword');

    if (newPassword !== confirmPassword) {
      // Handle password mismatch error
      return;
    }
    const encryptedPassword = await bcrypt.hash(newPassword, 12);
    try {
      // Make a PATCH request to update the user's information
      const response = await axios.patch(`http://localhost:4002/api/v1/users/${userId}`, {
        name: name,
        email: email,
        password: encryptedPassword,
      });

     
      console.log(response.data); 
      alert("Successfully updated")
    } catch (error) {
      
      console.log(error);
    }
  };

  return (
    <div className="DashAll">
      <NavbarD />

      <div className="HeadingAd1">
        <h1>Hello Admin</h1>
        <p>It's good to see you again!</p>
      </div>

      <form className="settingForm" onSubmit={handleSubmit}>
        <label htmlFor="name">Name:</label>
        <br />
        <input type="text" id="name" name="name" />
        <br />

        <label htmlFor="email">Email:</label>
        <br />
        <input type="email" id="email" name="email" />
        <br />

        <label htmlFor="password">New Password:</label>
        <br />
        <input type="password" id="password" name="password" />
        <br />

        <label htmlFor="Cpassword">Confirm Password:</label>
        <br />
        <input type="password" id="Cpassword" name="Cpassword" />
        <br />

        <input type="submit" id="setSubmit" value="Submit" />
      </form>
    </div>
  );
};

export default SettingDash;
