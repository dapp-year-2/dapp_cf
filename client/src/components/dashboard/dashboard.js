// import React, { useState } from 'react';
import './css/dashboard.css';
import Illustration from '../img/Illustration.png';
// import B1 from '../img/44.jpg';
import NavbarD from './navbarD';
import Modal from 'react-modal';
import React, { useEffect, useState } from 'react';
import axios from 'axios';

const Dashboard = (props) => {
  let allcampaigns = [];
  let completedcampaigns = [];
  let activecampaigns = [];
  for (var i = 0; i < props.allCampaigns.length; i++) {
    allcampaigns.push(props.allCampaigns[i])
    props.allCampaigns[i].completed === true && completedcampaigns.push(props.allCampaigns[i])
    props.allCampaigns[i].completed === false && activecampaigns.push(props.allCampaigns[i])
  }

  const [filteredData, setFilteredData] = useState(allcampaigns);
  const [activeCategory, setActiveCategory] = useState('All');
  const [modalIsOpen, setModalIsOpen] = useState(false);
  const [selectedItem, setSelectedItem] = useState(null);

  const handleCategoryClick = (category) => {
    setActiveCategory(category);

    if (category === 'All') {
      setFilteredData(allcampaigns);
    } else if (category === "Completed") {
      setFilteredData(completedcampaigns)
    } else {
      setFilteredData(activecampaigns);
    }
  };

  const modalStyles = {
    overlay: {
      // Styles for the overlay (background behind the modal)
      // ...
    },
    content: {
      // Styles for the modal content
      maxHeight: '80vh', // Set a maximum height for the content
      overflowY: 'auto', // Enable vertical scrolling
      // ...
    }
  }

  // const data1 = [
  //   { profilePic: B1, name: 'John Doe', email: 'john.doe@example.com' },
  //   { profilePic: Illustration, name: 'Jane Smith', email: 'jane.smith@example.com' },
  //   { profilePic: 'pic3.jpg', name: 'Alex Johnson', email: 'alex.johnson@example.com' },
  // ];

  // const userCount = data1.filter((item) => item).length;

  const openModal = (item) => {
    setSelectedItem(item);
    setModalIsOpen(true);
  };

  const closeModal = () => {
    setModalIsOpen(false);
  };
  const [users, setUsers] = useState([]);

  useEffect(() => {
    fetchUsers();
  }, []);

  const fetchUsers = async () => {
    try {
      const response = await axios.get('http://localhost:4002/api/v1/users');
      setUsers(response.data.data);
    } catch (error) {
      console.log(error);
    }
  };

  const handleDeleteUser = async (userId) => {
    const confirmed = window.confirm('Are you sure you want to delete this user?');
    if (confirmed) {
      try {
        await axios.delete(`http://localhost:4002/api/v1/users/${userId}`);
        setUsers((prevUsers) => prevUsers.filter((user) => user._id !== userId));
      } catch (error) {
        console.log(error);
      }
    }
  };

  const userCount1 = users.length;

  return (
    <div className="DashAll">
      <NavbarD />

      <div className="HeadingAd">
        <h1>Hello Admin</h1>
        <p>It's good to see you again!</p>
      </div>

      <img src={Illustration} id="imgill" alt='' />

      <div className="headingDD" id="hd1">
        <h1>{completedcampaigns.length}</h1>
        <p>Campaigns</p>
        <p>Completed</p>
      </div>

      <div className="headingDD" id="hd2">
        <strong>
          <h1>{activecampaigns.length}</h1>
        </strong>
        <p>Campaigns</p>
        <p>in progress</p>
      </div>

      <div className="headingDD" id="hd3">
        <strong>
          <h1>{userCount1}</h1>
        </strong>
        <p>Total</p>
        <p>User</p>
      </div>

      <h2 id="Ttitle1">Campaigns</h2>
      <div className="categories">
        <button
          className={activeCategory === 'All' ? 'active' : ''}
          onClick={() => handleCategoryClick('All')}
        >
          All
        </button>
        <button
          className={activeCategory === 'Completed' ? 'active' : ''}
          onClick={() => handleCategoryClick('Completed')}
        >
          Completed
        </button>
        <button
          className={activeCategory === 'Active' ? 'active' : ''}
          onClick={() => handleCategoryClick('Active')}
        >
          Active
        </button>
      </div>

      <table className="DashTable" id="CampaignTable">
        <thead>
          <tr>
            <th></th>
            <th>Title</th>
            <th>Status</th>
            <th></th>
          </tr>
        </thead>

        <tbody>
          {filteredData.map((item, index) => (
            <tr key={index}>
              <td>
                <img src={item.image} alt="Banner Img" style={{ width: '70px', height: '50px', borderRadius: '10px' }} />
              </td>
              <td>
                <h4 style={{ fontSize: "1rem" }}>{item.title} </h4>
              </td>
              <td>{item.status}</td>
              <td> <input type='button' value="View" onClick={() => openModal(item)} /> </td>
            </tr>
          ))}
        </tbody>
      </table>

      <h2 id="Ttitle2">User</h2>

      <table className="DashTable" id="UserTable">
        <thead>
          <tr>
            <th></th>
            <th>Name</th>
            <th></th>
          </tr>
        </thead>

        <tbody>
          {users.map((user, index) => (
            <tr key={index}>
              <td>
                {/* <img src={user.profilePic} alt="Profile" style={{ width: '70px', height: '50px', borderRadius: '10px' }} /> */}
              </td>
              <td>
                <h4>{user.name}</h4>
                <p>{user.email}</p>
              </td>
              <td> <button onClick={() => handleDeleteUser(user._id)}>Delete</button> </td>
            </tr>
          ))}
        </tbody>
      </table>

      <Modal
        isOpen={modalIsOpen}
        onRequestClose={closeModal}
        style={modalStyles}
        contentLabel="View Campaign"
        className="Modal"
      >
        {selectedItem && console.log(selectedItem)}
        {selectedItem && (
          <div>
            <h2>{selectedItem.title}</h2>
            <img src={selectedItem.image} alt="Banner Img" style={{ height: '280px', borderRadius: '10px' }} />
            {selectedItem.description.split(/\n|\n\n/).map((p, i) => (
              <p style={{ textAlign: "justify", paddingInline: "3rem" }} key={i}>{p}</p>
            ))}
            <p>Status: {selectedItem.status === true ? "Completed" : "Active"}</p>
            <button onClick={closeModal}>Close</button>
          </div>
        )}
      </Modal>


    </div>
  );
};

export default Dashboard;
