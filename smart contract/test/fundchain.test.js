const FundChain = artifacts.require('FundChain');

contract('FundChain', (accounts) => {
  let contractInstance;

  before(async () => {
    contractInstance = await FundChain.new();
  });

  it('should create a campaign', async () => {
    const title = 'Campaign Title';
    const description = 'Campaign Description';
    const targetAmount = 1000;
    const endDate = Math.floor(Date.now() / 1000) + 3600; // 1 hour from now
    const image = 'Campaign Image';

    const createCampaignTx = await contractInstance.createCampaign(title, description, targetAmount, endDate, image);

    const campaignID = createCampaignTx.logs[0].args.campaignID;
    const campaign = await contractInstance.getCampaign(campaignID);

    assert.equal(campaign.owner, accounts[0], 'Incorrect campaign owner');
    assert.equal(campaign.title, title, 'Incorrect campaign title');
    assert.equal(campaign.description, description, 'Incorrect campaign description');
    assert.equal(campaign.targetAmount, targetAmount, 'Incorrect campaign target amount');
    assert.equal(campaign.endDate, endDate, 'Incorrect campaign end date');
    assert.equal(campaign.amountRaised, 0, 'Incorrect campaign amount raised');
    assert.equal(campaign.completed, false, 'Incorrect campaign completion status');
    assert.equal(campaign.image, image, 'Incorrect campaign image');
    assert.deepEqual(campaign.donors, [], 'Incorrect campaign donors');
    assert.deepEqual(campaign.donations, [], 'Incorrect campaign donations');
  });

  it('should receive a donation to a campaign', async () => {
    const campaignID = 1;
    const donationAmount = 500;

    const donationTx = await contractInstance.donateToCampaign(campaignID, {
      from: accounts[1],
      value: donationAmount,
    });

    const campaign = await contractInstance.getCampaign(campaignID);

    assert.equal(campaign.amountRaised, donationAmount, 'Incorrect campaign amount raised');
    assert.deepEqual(campaign.donors, [accounts[1]], 'Incorrect campaign donors');
    assert.deepEqual(campaign.donations, [donationAmount], 'Incorrect campaign donations');
  });

  // Add more test cases as needed
  
});
