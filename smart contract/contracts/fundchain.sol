// SPDX-License-Identifier: MIT
pragma solidity ^0.8.17;

contract FundChain {
    struct Campaign {
        address payable owner;
        string title;
        string description;
        uint256 targetAmount;
        uint256 endDate;
        uint256 amountRaised;
        bool completed;
        string image;
        address[] donors;
        uint256[] donations;
    }

    mapping(uint256 => Campaign) public campaigns;
    uint256 public campaignCount = 0;

    event CampaignCreated(uint indexed campaignID, address indexed _owner);
    event DonationReceived(
        address indexed donor,
        uint indexed campaignID,
        uint amount
    );
    event CampaignCompleted(uint indexed campaignID);

    function createCampaign(
        string memory _title,
        string memory _description,
        uint256 _targetAmount,
        uint256 _endDate,
        string memory _image
    ) public {
        require(_targetAmount > 0, "Goal amount must be greater than 0");
        require(_endDate > block.timestamp, "Deadline must be in the future");
        campaignCount++;
        Campaign storage campaign = campaigns[campaignCount];
        campaign.owner = payable(msg.sender);
        campaign.title = _title;
        campaign.description = _description;
        campaign.targetAmount = _targetAmount;
        campaign.endDate = _endDate;
        campaign.amountRaised = 0;
        campaign.image = _image;
        emit CampaignCreated(campaignCount, msg.sender);
    }

    function donateToCampaign(uint256 _campaignID) public payable {
        require(msg.value > 0, "Donation amount must be greater than 0");
        Campaign storage campaign = campaigns[_campaignID];
        require(!campaign.completed, "Campaign is completed");
        require(
            block.timestamp < campaign.endDate,
            "Campaign deadline has passed"
        );
        campaign.donors.push(msg.sender);
        campaign.donations.push(msg.value);
        campaign.amountRaised += msg.value;
        emit DonationReceived(msg.sender, _campaignID, msg.value);
        if (campaign.amountRaised >= campaign.targetAmount) {
            campaign.completed = true;
            emit CampaignCompleted(_campaignID);
            (bool sent, ) = payable(campaign.owner).call{value: msg.value}("");
            if (sent) {
                campaign.amountRaised += msg.value;
            }
        }
    }

    function getdonors(
        uint256 _id
    ) public view returns (address[] memory, uint256[] memory) {
        return (campaigns[_id].donors, campaigns[_id].donations);
    }

    function getCampaign(
        uint _campaignID
    )
        public
        view
        returns (
            address,
            string memory,
            string memory,
            uint256,
            uint256,
            uint256,
            bool,
            string memory,
            address[] memory,
            uint256[] memory
        )
    {
        Campaign storage campaign = campaigns[_campaignID];
        return (
            campaign.owner,
            campaign.title,
            campaign.description,
            campaign.targetAmount,
            campaign.amountRaised,
            campaign.endDate,
            campaign.completed,
            campaign.image,
            campaign.donors,
            campaign.donations
        );
    }

    function getCampaigns() public view returns (Campaign[] memory) {
        Campaign[] memory allCampaigns = new Campaign[](campaignCount);
        for (uint i = 0; i < campaignCount; i++) {
            Campaign storage item = campaigns[i];
            allCampaigns[i] = item;
        }
        return allCampaigns;
    }
}
